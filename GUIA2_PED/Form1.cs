﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GUIA2_PED
{
    public partial class Form1 : Form
    {

        //declaracion de variables
        int x, y;//variables globales que permiten determinar ubicacion del click
        //colores RGB
        int r, gr, b;

        public Form1()
        {
            InitializeComponent();
            //asignando borde al panel para poder visualizarlo en el form
            panel1.BorderStyle = BorderStyle.FixedSingle;
        }
        Random rndR = new Random();
        Random rndG = new Random();
        Random rndB = new Random();
        private void panel1_MouseClick(object sender, MouseEventArgs e)
        {
            //MouseEventArgs e brinda informacion sobre los eventos con el mouse
            Point punto = new Point(e.X, e.Y);//creamos una instacia de la Struct point, esto dara las coordenadas x,y del mouse
            //se toman las coordenadas del obj punto y se almacenan en las variables
            x = punto.X;
            y = punto.Y;

            //consiguiendo numeros aleatorios para volores;
            
            r = rndR.Next(255);
            
            gr = rndG.Next(255);
            
            b = rndB.Next(255);

            //MessageBox.Show("r: " + r + ", g: " + gr + ", b: " + b);

            panel1.Invalidate();
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = panel1.CreateGraphics();//estableciendo zona para dibujar(importante)
            Pen lapiz = new Pen(Color.Black);

            //instanciando color
            Color clr = new Color();
            clr = Color.FromArgb(r, r, r);

            if (listBox1.SelectedIndex == 0)//si selecciona circulo
            {
                LinearGradientBrush linear = new LinearGradientBrush(
                   new Point(0, 0),
                   new Point(500, 0),
                   Color.FromArgb(r, gr, 0, 0),
                   Color.FromArgb(r, 0, 0, gr));

                g.DrawEllipse(lapiz, x - 50, y - 50, gr, b);//x,y,width, height
                g.FillEllipse(linear, x - 50, y - 50, gr, b);//x,y,width, height
            }
            if(listBox1.SelectedIndex == 1)//si selecciona rectangulo
            {
                LinearGradientBrush linear = new LinearGradientBrush(
                   new Point(0, 10),
                   new Point(200, 10),
                   Color.FromArgb(r, gr, 0, 0),  
                   Color.FromArgb(r, 0, 0, gr));
                
                g.DrawRectangle(lapiz, x - 50, y - 50, r, b);//x,y,width, height
                g.FillRectangle(linear, x - 50, y - 50, r, b);//x,y,width, height
            }
        }
    }
}
