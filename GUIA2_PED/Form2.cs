﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GUIA2_PED
{
    public partial class Form2 : Form
    {
        Graphics area;//area de trabajo
        public Form2()
        {            
            InitializeComponent();
            area = areadibujo.CreateGraphics();//establezco el área de dibujo para el picturebox            
        }

        List<Point> Listpuntos = new List<Point>();
        int z = 0;//contadora
        private void areadibujo_MouseClick(object sender, MouseEventArgs e)
        {
            Point punto = new Point(e.X, e.Y);
            if (z > 2)//reinicia contadora y limpia la lista
            {
                z = 0;
                Listpuntos.Clear();
                MessageBox.Show("Reiniciando...");
                area.Clear(Color.White);//limpia area a blanco, para que lineas no se vean una sobre otra
            }
            else//guarda los puntos en la lista
            {                
                Listpuntos.Add(punto);
            }
            z++;
        }

        //declaracion de variables
        int puntoinicio;        
        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                Pen lapicero = new Pen(Color.Black);//color por defecto

                switch (cmbcolor.SelectedIndex)//colorear dependiendo de lo seleccionado en cmb
                {
                    case 0: lapicero = new Pen(Color.Yellow); break;//seleccion amarilla
                    case 1: lapicero = new Pen(Color.Red); break;//seleccion roja
                    case 2: lapicero = new Pen(Color.Blue); break;//seleccion azul
                    case 3: lapicero = new Pen(Color.Black); break;//seleccion negra
                }

                int iteraciones = int.Parse(txtcantidad.Text);//cantidad de lineas a dibujar
                int espacio = int.Parse(txtespaciado.Text);//espaciado asignado(en pixeles)

                area.Clear(Color.White);//limpia area a blanco, para que lineas no se vean una sobre otra

                puntoinicio = 50;//inicio en un valor de y=50

                for (int i = 0; i < iteraciones; i++)
                {
                    //obteniendo primer y ultimo punto almacenado en la lista
                    var inicio = Listpuntos.First();
                    var fin = Listpuntos.Last();
                    area.DrawLine(lapicero, inicio.X, inicio.Y + (espacio * i), fin.X, fin.Y + (espacio * i));
                    //dibuja linea por linea de acuerdo al color dado, en x van de 20 a 300, y en Y varia segun la iteracion
                }
            }
            catch
            {
                MessageBox.Show("Debes seleccionar dos puntos para dibujar las lineas");
            }            
        }

        private void txtcantidad_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsNumber(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Decimal.TryParse(txtcantidad.Text, out decimal result))
            {
                e.Handled = true;
                MessageBox.Show("Solo se permiten números enteros");
            }
            else
            {
                e.Handled = true;
                MessageBox.Show("Solo se permiten números enteros");
            }
        }

        private void txtespaciado_KeyPress(object sender, KeyPressEventArgs e)
        {            
            if (Char.IsNumber(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Decimal.TryParse(txtespaciado.Text, out decimal result))
            {
                e.Handled = true;
                MessageBox.Show("Solo se permiten números enteros");
            }
            else
            {
                e.Handled = true;
                MessageBox.Show("Solo se permiten números enteros");
            }
        }
    }
}
