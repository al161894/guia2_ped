﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GUIA2_PED
{
    public partial class Form3 : Form
    {
        //definimos un set de constantes que pueden ser asignados a una variable
        enum Posicion
        {
            izquierda, derecha, arriba, abajo
        }
        //definimos propiedades para posicion
        private int x;
        private int y;
        private Posicion objposicion; //variable enum posicion

        public Form3()
        {
            InitializeComponent();
            x = 50;//inicializa en x = 50
            y = 50;//inicializa en y = 50
            objposicion = Posicion.abajo; //por defecto definimos que se mueve hacia abajo
        }

        private void timermov_Tick(object sender, EventArgs e)
        {
            if(objposicion == Posicion.derecha)
            { x += 10;  }//desplazarse 10 px a la derecha
            else if(objposicion == Posicion.izquierda)
            { x -= 10; }//desplazarse 10 px a la izquierda
            else if(objposicion == Posicion.arriba)
            { y -= 10; }//desplazarse 10 px hacia arriba
            else if (objposicion == Posicion.abajo)
            { y += 10; }//desplazarse 10 px hacia abajo

            Invalidate();//invalida la superficie del control y hace que se vuelva a dibujar el control
        }

        private void Form3_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Left)//si se presiona la tecla flecha izquierda
            {
                objposicion = Posicion.izquierda;
            }
            else if(e.KeyCode == Keys.Right)//si se presiona la tecla flecha derecha
            {
                objposicion = Posicion.derecha;
            }
            else if(e.KeyCode == Keys.Up)//si se presiona la tecla flecha arriba
            {
                objposicion = Posicion.arriba;
            }
            else if(e.KeyCode == Keys.Down)//si se presiona la tecla flecha abajo
            {
                objposicion = Posicion.abajo;
            }
        }

        private void Form3_Paint(object sender, PaintEventArgs e)
        {
            //para evitar el parpadeo del fondo se establece una propiedad doubleBuffered como true             
            e.Graphics.DrawImage(new Bitmap("Mario_8_bit.png"), x, y, 60, 60);//x, y, width, height
            //se dibuja la imagen agregada al proyecto y se establece el punto inicial y tamaño

            e.Graphics.DrawImage(new Bitmap("hongo.png"), 100, 100, 55, 55);
        }
        
        
    }
}
